<?php


namespace ShopExpress\BarcodeGenerator;


use Symfony\Component\Filesystem\Filesystem;

/**
 * Class QrCode
 * @package ShopExpress\BarcodeGenerator
 */
class QrCode
{
    /**
     * @var string
     */
    private $delimiter;
    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * @var string
     */
    private $folder;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $personalAcc;
    /**
     * @var string
     */
    private $bankName;
    /**
     * @var string
     */
    private $bik;
    /**
     * @var string
     */
    private $corrAcc;
    /**
     * @var string
     */
    private $inn;
    /**
     * @var string
     */
    private $kpp;
    /**
     * @var string
     */
    private $purpose;
    /**
     * @var string
     */
    private $sum;
    /**
     * @var string
     */
    private $payerLastName;
    /**
     * @var string
     */
    private $payerFirstName;
    /**
     * @var string
     */
    private $payerMiddleName;
    /**
     * @var string
     */
    private $payerAddress;
    /**
     * @var string
     */
    private $payerPhone;
    /**
     * @var array
     */
    private $required = [
        'name', 'bankName', 'personalAcc', 'bik', 'corrAcc'
    ];

    /**
     * QrCode constructor.
     *
     * @param Filesystem $filesystem
     * @param string $folder
     * @param string $delimiter
     */
    public function __construct(Filesystem $filesystem, string $folder = '', string $delimiter = '|')
    {
        $this->filesystem = $filesystem;
        $this->delimiter = $delimiter;
        $this->folder = rtrim($folder, '/');
    }

    /**
     * @param int $size
     *
     * @return string|null
     */
    public function generate(int $size = 300): ?string
    {
        foreach ($this->required as $field) {
            if (empty($this->{$field})) {
                throw new \InvalidArgumentException(sprintf('Parameter `%s` is required.', $field));
            }
        }

        $requiredBlock = [
            'Name' => $this->name,
            'PersonalAcc' => $this->personalAcc,
            'BankName' => $this->bankName,
            'BIC' => $this->bik,
            'CorrespAcc' => $this->corrAcc,
        ];

        $requiredBlock = $this->arrayToString($requiredBlock, $this->delimiter);

        $additionalBlock = [
            'PayeeINN' => $this->inn,
            'Sum' => $this->sum * 100,
            'Purpose' => $this->purpose,
            'LastName' => $this->payerLastName,
            'FirstName' => $this->payerFirstName,
            'MiddleName' => $this->payerMiddleName,
            'PayerAddress' => $this->payerAddress,
            'Phone' => $this->payerPhone,
        ];

        $additionalBlock = $this->arrayToString($additionalBlock, $this->delimiter);

        $string = 'ST00012' . $this->delimiter . $requiredBlock . $additionalBlock;
        $string = substr($string, 0, -1);

        $response = $this->request('https://chart.googleapis.com/chart', [
            'cht' => 'qr',
            'choe' => 'UTF-8',
            'chl' => $string,
            'chs' => $size . 'x' . $size,
            'chld' => 'M|4',
        ]);

        if (!$response || strpos($response, 'Error')) {
            return null;
        }

        $path = '/qrcode_' . substr(hash('sha1', $string), 0, 6) . '.png';
        $this->filesystem->dumpFile($this->folder . $path, $response);

        return $path;
    }

    /**
     * @param array $array
     * @param string $del
     * @param string $eq
     *
     * @return string
     */
    private function arrayToString(array $array, string $del = '|', string $eq = '='): string
    {
        $string = '';
        if (is_array($array)) {
            foreach ($array as $key => $value) {
                if (!empty($value)) {
                    $string .= $key . $eq . $value . $del;
                }
            }
        }

        return $string;
    }

    /**
     * @param string $name
     *
     * @return QrCode
     */
    public function setName(string $name): QrCode
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param string $personalAcc
     *
     * @return QrCode
     */
    public function setPersonalAcc(string $personalAcc): QrCode
    {
        $this->personalAcc = $personalAcc;
        return $this;
    }

    /**
     * @param string $bankName
     *
     * @return QrCode
     */
    public function setBankName(string $bankName): QrCode
    {
        $this->bankName = $bankName;
        return $this;
    }

    /**
     * @param string $bik
     *
     * @return QrCode
     */
    public function setBik(string $bik): QrCode
    {
        $this->bik = $bik;
        return $this;
    }

    /**
     * @param string $corrAcc
     *
     * @return QrCode
     */
    public function setCorrAcc(string $corrAcc): QrCode
    {
        $this->corrAcc = $corrAcc;
        return $this;
    }

    /**
     * @param string $inn
     *
     * @return QrCode
     */
    public function setInn(string $inn): QrCode
    {
        $this->inn = $inn;
        return $this;
    }

    /**
     * @param string $kpp
     *
     * @return QrCode
     */
    public function setKpp(string $kpp): QrCode
    {
        $this->kpp = $kpp;
        return $this;
    }

    /**
     * @param string $purpose
     *
     * @return QrCode
     */
    public function setPurpose(string $purpose): QrCode
    {
        $this->purpose = $purpose;
        return $this;
    }

    /**
     * @param string $sum
     *
     * @return QrCode
     */
    public function setSum(string $sum): QrCode
    {
        $this->sum = $sum;
        return $this;
    }

    /**
     * @param string $payerLastName
     *
     * @return QrCode
     */
    public function setPayerLastName(string $payerLastName): QrCode
    {
        $this->payerLastName = $payerLastName;
        return $this;
    }

    /**
     * @param string $payerFirstName
     *
     * @return QrCode
     */
    public function setPayerFirstName(string $payerFirstName): QrCode
    {
        $this->payerFirstName = $payerFirstName;
        return $this;
    }

    /**
     * @param string $payerMiddleName
     *
     * @return QrCode
     */
    public function setPayerMiddleName(string $payerMiddleName): QrCode
    {
        $this->payerMiddleName = $payerMiddleName;
        return $this;
    }

    /**
     * @param string $payerAddress
     *
     * @return QrCode
     */
    public function setPayerAddress(string $payerAddress): QrCode
    {
        $this->payerAddress = $payerAddress;
        return $this;
    }

    /**
     * @param string $payerPhone
     *
     * @return QrCode
     */
    public function setPayerPhone(string $payerPhone): QrCode
    {
        $this->payerPhone = $payerPhone;
        return $this;
    }

    /**
     * @param string $url
     * @param array $params
     *
     * @return bool|string
     */
    private function request(string $url, array $params = [])
    {
        $ch = curl_init();

        curl_setopt_array($ch, [
            CURLOPT_URL => $url . '?' . http_build_query($params),
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_USERAGENT => 'User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.12) Gecko/20101026 Firefox/3.6.12',
        ]);

        $result = curl_exec($ch);

        curl_close($ch);

        return $result;
    }
}